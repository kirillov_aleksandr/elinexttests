package test.tutby;

import model.MainPage;
import model.SearchResultPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;


public class Tests extends TestBase {

  @Test
  public void searchWithSelenium() {
    MainPage mainPage = PageFactory.initElements(app.driver, MainPage.class);
    mainPage.typeSearchQuery("Лукашенко");
    mainPage.clickSearchButton();
  }
}