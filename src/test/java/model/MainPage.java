package model;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MainPage {
  private WebDriver driver;

  public MainPage(WebDriver driver) {
    this.driver = driver;
  }

  @FindBy(id = "search_from_str")
  private WebElement searchBar;

  @FindBy(name = "search")
  private WebElement searchButton;

  public SearchResultPage clickSearchButton() {
    searchButton.click();
    return new SearchResultPage(driver);
  }

  public MainPage typeSearchQuery(String searchQuery) {
    searchBar.sendKeys(searchQuery);
    return this;
  }
}
