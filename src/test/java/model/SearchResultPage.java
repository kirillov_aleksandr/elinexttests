package model;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchResultPage {
  private WebDriver driver;

  public SearchResultPage(WebDriver driver) {
    this.driver = driver;
  }

  @FindBy(xpath = "//li[1]/h3/a[2]")
  private WebElement searchResult1;

  @FindBy (xpath = "//li[2]/h3/a[2]")
  private WebElement searchResult2;

  @FindBy (xpath = "//li[3]/h3/a[2]")
  private WebElement searchResult3;

  @FindBy (xpath = "//li[5]/h3/a[2]")
  private WebElement searchResult4;

  @FindBy (xpath = "//li[6]/h3/a[2]")
  private WebElement searchResult5;

  @FindBy (xpath = "//li[7]/h3/a[2]")
  private WebElement searchResult6;

  @FindBy (xpath = "//li[8]/h3/a[2]")
  private WebElement searchResult7;

  @FindBy (xpath = "//li[10]/h3/a[2]")
  private WebElement searchResult8;

  @FindBy (xpath = "//li[11]/h3/a[2]")
  private WebElement searchResult9;

  @FindBy (xpath = "//li[12]/h3/a[2]")
  private WebElement searchResult10;

  @FindBy (xpath = "//li[13]/h3/a[2]")
  private WebElement searchResult11;

  @FindBy (xpath = "//li[14]/h3/a[2]")
  private WebElement searchResult12;

  @FindBy (xpath = "//li[15]/h3/a[2]")
  private WebElement searchResult13;
}
